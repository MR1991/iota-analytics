use anyhow::anyhow;
use anyhow::Result;
use bee_message::{output::Output, output::OutputId};
use serde::{Deserialize, Serialize};

use std::collections::HashMap;
use std::{fs::File, io::BufWriter, path::Path};
use std::env;

mod snapshot;
use snapshot::get_snapshot_data;

const OUTPUT_DIR: &str = "jsons";
const BECH_32_HRP: &str = "iota";

// Data of all addresses and the treasury for a certain ledger_index
#[derive(Clone, Serialize, Deserialize)]
pub struct SnapshotData {
    // The milestone index
    #[serde(rename = "ledgerIndex")]
    ledger_index: u32,
    // All outputs with balance and address bech32 encoded
    output_hashmap: HashMap<OutputId, OutputData>,
    // The remaining amount in the treasury (legacy network)
    #[serde(rename = "treasuryOutputAmount")]
    treasury_output_amount: u64
}

/// Data for an output
#[derive(Clone, Serialize, Deserialize)]
pub struct OutputData {
    // The total amount of the output
    pub balance: u64,
    // The adress for the output
    pub address: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let snapshot_date = &args[1];
    let filename = &args[2];

    let snapshot_data = read_snapshot_addresses_data(snapshot_date)?;
    write_to_file(
        format!("{}/{}.json", OUTPUT_DIR, filename),
        snapshot_data,
    )?;
    Ok(())
}

// Read snapshot file and write addresses with their balance to a file in json format
fn read_snapshot_addresses_data(snapshot_path: &str) -> Result<SnapshotData> {
    let mut output_hashmap = HashMap::new(); // create new mutable hashmap
    let (outputs, treasury_output_amount, ledger_index) = get_snapshot_data(snapshot_path)?; // get all data from the snapshot in format Vec<OutputData>, u64, u32)
    for output_data in outputs.into_iter() {
        let (amount, address) = get_output_amount_and_address(&output_data.output)?;
        output_hashmap
            .entry(output_data.output_id)
            .or_insert_with(|| OutputData {
                balance: amount,
                address: address,
            });
    }
    Ok(SnapshotData {
        ledger_index,
        output_hashmap,
        treasury_output_amount
    })
}

/// Get output amount and address from an Output
pub fn get_output_amount_and_address(output: &Output) -> Result<(u64, String)> {
    let (amount, address) = match output {
        Output::Treasury(_) => return Err(anyhow!("Treasury output is not allowed")),
        Output::SignatureLockedSingle(ref r) => (r.amount(), r.address()),
        Output::SignatureLockedDustAllowance(ref r) => (r.amount(), r.address()),
    };
    Ok((amount, address.to_bech32(BECH_32_HRP)))
}

/// Function to write address snapshot data to a file
pub fn write_to_file<P: AsRef<Path>>(path: P, snapshot_data: SnapshotData) -> Result<()> {
    let jsonvalue = serde_json::to_value(&snapshot_data)?; // Converts to json
    let file = File::create(path)?; // Creates file
    let bw = BufWriter::new(file); // Opens writer to file?
    serde_json::to_writer_pretty(bw, &jsonvalue)?; // Writes json to file
    Ok(())
}
