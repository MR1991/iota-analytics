import os
import shutil

def absolute_file_paths(directory):
    path = os.path.abspath(directory)
    return [entry.path for entry in sorted(os.scandir(path), key=lambda e: e.name) if entry.is_file()]

file_paths = absolute_file_paths("single_snapshot")

for i in file_paths:
    print(i)
    dest_dir = os.getcwd()
    src_file = i
    shutil.copy(src_file, dest_dir)
    filepath = os.path.basename(i)
    filename = os.path.basename(i).split('.')[0]
    os.system("cargo run "+filepath+" "+filename)

directory = "."
files_in_directory = os.listdir(directory)
filtered_files = [file for file in files_in_directory if file.endswith(".bin")]
for file in filtered_files:
	path_to_file = os.path.join(directory, file)
	os.remove(path_to_file)
